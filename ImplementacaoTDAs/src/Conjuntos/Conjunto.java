package Conjuntos;

import java.util.List;

public interface Conjunto<T> {
    boolean adicionar(T e);
    boolean remover(T e);
    boolean existe(T e);
    T referenciaPara(T e);

    int qtdeElems();

    // Métodos mutáveis, o resultado está na própria estrutura de dados
    // envolvida.
    default void uniao(Conjunto<T> s) {
        for (T elem : s.todosElementos()) {
            if (!this.existe(elem)) {
                this.adicionar(elem);
            }
        }
    }

    void intersecao(Conjunto<T> s);
    void diferenca(Conjunto<T> s);

    // Métodos imutáveis, não modificam nenhuma das estruturas, e retornam
    // uma nova estrutura de dados, resultante semântica da operação.
    Conjunto<T> produzirUniao(Conjunto<T> s);
    Conjunto<T> produzirIntersecao(Conjunto<T> s);
    Conjunto<T> produzirDiferenca(Conjunto<T> s);

    List<T> todosElementos();
}
