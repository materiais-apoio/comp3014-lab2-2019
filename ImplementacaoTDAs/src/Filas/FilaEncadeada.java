package Filas;

public class FilaEncadeada<T> implements Fila<T> {
    private No primeiro;
    private No ultimo;
    private int qtdeElems;

    @Override
    public void enfileirar(T elem) {
        No enfileirado = new No(elem);
        if (primeiro == null) {
            primeiro = enfileirado;
        }
        if (ultimo != null) {
            ultimo.proximo = enfileirado;
        }
        ultimo = enfileirado;
    }

    @Override
    public T desenfileirar() {
        T desenfileirado = primeiro.valor;
        primeiro = primeiro.proximo;
        return desenfileirado;
    }

    @Override
    public T proximoElem() {
        return primeiro.valor;
    }

    @Override
    public T ultimoElem() {
        return ultimo.valor;
    }

    @Override
    public int qtdeElems() {
        return 0;
    }

    private class No {
        No proximo = null;
        T valor;

        No(T valor) { this.valor = valor; }
    }
}
