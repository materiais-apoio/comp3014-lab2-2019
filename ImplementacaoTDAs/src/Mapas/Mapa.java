package Mapas;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface Mapa<K, V> {
    Optional<V> acessar(K indexador);
    default V acessarOuComum(K indexador, V comum) {
        return existe(indexador)
                ? acessar(indexador).get()
                : comum;
    }

    void gravar(K indexador, V valor);
    default void gravarSeInexistente(K indexador, V valor) {
        if (!existe(indexador))
            gravar(indexador, valor);
    }

    boolean existe(K indexador);

    List<K> indexadores();

    default List<V> valores() {
        List<V> valores = new ArrayList<V>();
        for (K i : indexadores()) {
            valores.add(acessar(i).get());
        }
        return valores;
    }
}
