package Mapas;

import Conjuntos.Conjunto;

import java.util.List;
import java.util.Optional;

public class MapaEspalhamento<K, V> implements Mapa<K, V> {

    @Override
    public Optional<V> acessar(K indexador) {
        if (existe(indexador)) {
            V valor = mapa.referenciaPara(
                    new Associacao<>(indexador, null)).valor;
            return Optional.of(valor);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void gravar(K indexador, V valor) {
        mapa.adicionar(new Associacao<>(indexador, valor));
    }

    @Override
    public boolean existe(K indexador) {
        return mapa.existe(new Associacao<>(indexador, null));
    }

    @Override
    public List<K> indexadores() {
        return null;
    }

    private Conjunto<Associacao<K, V>> mapa;

    class Associacao<K, V> {
        K indexador;
        V valor;

        Associacao(K i, V v) {
            indexador = i;
            valor = v;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Associacao) {
                Associacao<K, V> outro = (Associacao<K,V>) o;
                return this.indexador.equals(outro.indexador);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return indexador.hashCode();
        }
    }
}
