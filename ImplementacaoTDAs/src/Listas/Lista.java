package Listas;

public interface Lista<T> {
    T consultar(int pos);
    void sobrescrever(T elem, int pos);
    void inserir(T elem, int pos);
    T remover(int pos);
    int procurarPor(T elem);
}
