package br.unifil.dc.lab2;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        // Realiza benchmarkings
        final int inicio = 500, limite = 10001, passo = 500, repeticoes = 10;
        List<Medicao> medicoesBolha = Benchmarking.benchmarkIntervalo(
                inicio, limite, passo, repeticoes,
                Ordenadores::bubblesort);

        List<Medicao> medicoesSelecao = Benchmarking.benchmarkIntervalo(
                inicio, limite, passo, repeticoes,
                Ordenadores::selectionsort);


        // Plotta gráfico com resultados levantados
        TabelaTempos tt = new TabelaTempos();
        tt.setTitulo("Tempo para ordenação");
        tt.setEtiquetaX("Qtde elementos lista");
        tt.setEtiquetaY("Tempo (s)");
        tt.setLegendas("Bubble", "Selection");
        for (int i = 0; i < medicoesBolha.size(); i++) {
            Medicao amostraBolha = medicoesBolha.get(i);
            Medicao amostraSelecao = medicoesSelecao.get(i);

            tt.anotarAmostra(amostraBolha.getDominioAmostra(),
                    amostraBolha.getTempoSegundos(),
                    amostraSelecao.getTempoSegundos());
        }
        tt.exibirGraficoXY();
    }
}
