# Projeto - Análise de desempenho de algoritmos por benchmarking e tempo de execução

No repositório GitLab da disciplina, baixe o projeto ProjAnaliseTemposAlgoritmos, que contém a estrutura de código a ser usada como ponto de partida, e configure-o para utilização da biblioteca **JFreeChart** e sua dependência **JCommon** [1]. Em seguida, cumpra as seguintes atividades:

* Escreva os **javadocs** para todas as classes e métodos pré-existentes neste projeto que já não os possua. Escreva-os também para todos aqueles que vir a implementar.

* Há no projeto uma classe `Cronometro` especificada com assinaturas de métodos e **javadocs**, cujo objetivo principal é encapsular a funcionalidade de `System.nanoTime()`, afim de facilitar seu uso e evitar erros. Implemente essa classe e coloque-a em uso nos métodos que estão utilizando `System.nanoTime()`, fazendo as adaptações necessárias para o correto funcionamento do benchmarking.


* Utilizando a classe `TabelaTempos`, gere um gráfico _Tempo (ms) x lista.size()_ para cada situação descrita a seguir:
   * Comparação entre seleção, bolha e inserção com listas crescentes.
   * Comparação entre seleção, bolha e inserção com listas decrescentes.
   * Comparação entre inserção e mergesort com listas crescentes.
   * Comparação entre inserção e mergesort com listas decrescentes.
   * Comparação entre mergesort e quicksort com listas decrescentes.
   * Comparação entre mergesort e quicksort com listas crescentes.
Para cada gráfico, salve como imagem `.PNG`.

* O mergesort mais simples, implementado e utilizado nas medições do exercício anterior, subdivide o problema até chegar a listas com apenas 1 elemento, quando estarão por definição ordenadas. Apesar de funcionar bem, essa operação de subdivisão é muito custosa quando tratamos de listas enormes, uma lista com 1 milhão de elementos sofrerá tantas subdivisões até se tornar 1 milhão de listas com apenas um elemento cada.

Uma maneira melhor de implementar mergesort é subdividir as listas até termos sublistas com tamanho arbitrário _k_, que então são submetidas à ordenação por inserção, e então reagregadas por intercalação normalmente.

Com o objetivo de adotar e analisar essa abordagem, faça o que se pede:
   1. Implemente `insertionsort(vals, e, d)`, quer ordena _in-place_ a lista `vals`, de seu índice `e` até o índice `d` (excluso).
   2. Modifique a implementação de `mergesort` para que utilize um fator `k` de decisão para subdivir a ordenação (se `size()` for maior que `k`) ou se vai ordenar a lista com `insertionsort` (se `size()` for menor que `k`).
   3. Utilize `TabelaTempos` para plottar uma comparação entre `mergesort` com `k` igual a 1, 2, 8, 16 e 32.


* O algoritmo de `quicksort` tem diversas implementações com implicações diretas no seu desempenho. O principal ponto de variação de implementação é o uso de algoritmos distintos na implementação do método de particionamento. Neste projeto, há as implementações por _Hoare_ e _Lomuto_. Utilize `TabelaTempos` para demonstrar qual das duas é mais rápida em seu computador.


* Análise de desempenho formal:
   1. Demonstre os tempos de execução $`T(n)`$ dos métodos de ordenação `bubblesort`, `selectionsort` e `insertionsort`.
   2. Classifique todos os algoritmos de ordenação trabalhados nesse quanto a complexidade assintótica para os conjuntos $`Big-O`$, $`\Omega`$ e $`\Theta`$.


* **Entrega!** Elabore um relatório que inclua apresentação das medições feitas, os gráficos, a análise de tempo de execução e a comparação dos gráficos entre si, identificando as similaridades, diferenças, e as conclusões que pode-se tomar a partir dessa experimentação. Entregue em `PDF`.


## Referências
1. Object Refinery Limited. JFreeChart. Disponível em: http://www.jfree.org/jfreechart/. Último acesso: _7 de maio de 2019_.
