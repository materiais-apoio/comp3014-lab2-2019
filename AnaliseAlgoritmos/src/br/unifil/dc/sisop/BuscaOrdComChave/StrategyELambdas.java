package br.unifil.dc.sisop.BuscaOrdComChave;

import java.util.List;

public class StrategyELambdas {

    private enum OrdemClassificacao {
        CRESCENTE,
        DECRESCENTE
    }

    /**
     * Ordena decrescentemente elems pelo algoritmo da bolha,
     * in-place.
     *
     * ATENCAO: O método modifica elems!
     *
     * @param elems lista a ser ordenada in-place, mutável.
     */
    public static void ordenarDecrescenteBolha(List<Integer> elems) {
        ordenarBolhaEnum(elems, OrdemClassificacao.DECRESCENTE);
    }

    /**
     * Ordena crescentemente elems pelo algoritmo da bolha,
     * in-place.
     *
     * ATENCAO: O método modifica elems!
     *
     * @param elems lista a ser ordenada in-place, mutável.
     */
    public static void ordenarCrescenteBolha(List<Integer> elems) {
        ordenarBolhaEnum(elems, OrdemClassificacao.CRESCENTE);
    }

    /**
     * Não faça mais esse tipo de método, utilize funções anônimas!
     */
    private static void ordenarBolhaEnum(List<Integer> elems, OrdemClassificacao tipo) {
        boolean houvePermuta;
        do {
            houvePermuta = false;
            for (int i = 1; i < elems.size(); i++) {
                final Integer esq = elems.get(i-1);
                final Integer dir = elems.get(i);

                if (esq.compareTo(dir) < 0
                        && tipo == OrdemClassificacao.DECRESCENTE) {
                    permutar(elems, i-1, i);
                    houvePermuta = true;
                } else if (esq.compareTo(dir) > 0
                        && tipo ==OrdemClassificacao.CRESCENTE) {
                    permutar(elems, i-1, i);
                    houvePermuta = true;
                }
            }
        } while (houvePermuta);
    }


    public static void ordenarCrescenteBolhaStrategy(
            List<Integer> elems) {

        // Closure em método anônimo
        Comparador<Integer> cmp = new Comparador<Integer>() {
            @Override
            public int comparar(Integer l, Integer d) {
                if (l.equals(d)) return 0;
                else if (l.compareTo(d) > 0) return -1;
                else return 1;
            }
        };
        ordenarBolhaStrategy(elems, cmp);
    }

    public static void ordenarDecrescenteBolhaStrategy(
            List<Integer> elems) {

        ordenarBolhaStrategy(elems, (l, d) -> l.compareTo(d) * -1);
    }

    private static <T> void ordenarBolhaStrategy(List<T> elems, Comparador<T> cmp) {
        boolean houvePermuta;
        do {
            houvePermuta = false;
            for (int i = 1; i < elems.size(); i++) {
                final T esq = elems.get(i-1);
                final T dir = elems.get(i);
                if (cmp.comparar(esq,dir) > 0) {
                    permutar(elems, i-1, i);
                    houvePermuta = true;
                }
            }
        } while (houvePermuta);
    }

    private static <T> void permutar(List<T> elems, int i, int j) {
        T held = elems.get(i); // held = elems[i];
        elems.set(i, elems.get(j)); // elems[i] = elems[j];
        elems.set(j, held); // elems[j] = held;
    }
}
