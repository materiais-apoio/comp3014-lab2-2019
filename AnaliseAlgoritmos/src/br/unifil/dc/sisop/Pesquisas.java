package br.unifil.dc.sisop;

import br.unifil.dc.sisop.Uteis;

import java.util.List;
import java.util.Optional;

public class Pesquisas {

    /**
     * Realiza uma pesquisa binária (muito rápida), do índice de
     * k em elems. Se encontrar, retorna o índice, senão -1.
     *
     * Limitação: Pressupões que elems esteja ordenado
     * crescentemente.
     *
     * @param elems Lista onde k será procurado.
     * @param k Elemento a ser procurado em elems.
     * @return índice de k em elems, senão -1.
     */
    public static int pesquisaBinariaRecursiva(List<Integer> elems, Integer k) {
        assert Uteis.isCrescente(elems) : "Pesquisa binária somente em listas crescentes.";

        // Caso base
        if (elems.isEmpty()) return -1;

        final int meio = elems.size() / 2;
        final int comparados = elems.get(meio).compareTo(k);

        if (comparados < 0) {
            List<Integer> metadeDireita =
                    elems.subList(meio+1, elems.size());
            final int idx = pesquisaBinariaRecursiva(metadeDireita, k);
            return idx < 0 ? idx : meio + idx;
        } else if (comparados > 0){
            List<Integer> metadeEsquerda =
                    elems.subList(0, meio);
            return pesquisaBinariaRecursiva(metadeEsquerda, k);
        } else {
            return meio;
        }
    }

    /**
     * Realiza uma pesquisa binária (muito rápida), do índice de
     * k em elems. Se encontrar, retorna o índice, senão vazio.
     *
     * Limitação: Pressupões que elems esteja ordenado
     * crescentemente.
     *
     * @param elems Lista onde k será procurado.
     * @param k Elemento a ser procurado em elems.
     * @return índice de k em elems, senão vazio.
     */
    public static Optional<Integer> pesquisaBinaria(List<Integer> elems, Integer k) {
        assert Uteis.isCrescente(elems) : "Pesquisa binária somente em listas crescentes.";

        int l = 0, r = elems.size();
        while (l < r) {
            final int meio = (l + r) / 2;
            final int comparados = elems.get(meio).compareTo(k);
            if (comparados < 0) {
                l = meio + 1;
            } else if (comparados > 0) {
                r = meio;
            } else { // comparados == 0
                return Optional.of(meio);
            }
        }

        return Optional.empty();
    } // O(log_2 n)

    /**
     * Faz pesquisa por um elemento em uma lista, indicando sua
     * posição na lista, se houver o elemento. Se o elemento não
     * existir, retorna vazio.
     *
     * Se houver mais de um k em elems, o índice do primeiro é
     * retornado, partindo de 0 para elems.size().
     *
     * @param elems Lista onde k será procurado.
     * @param k Elemento a ser procurado em elems.
     * @return índice de k em elems, senão vazio.
     */
    public static Optional<Integer> pesquisaSequencial(List<Integer> elems, Integer k) {
        for (int i = 0; i < elems.size(); i++) { // (1*n)+1
            if (elems.get(i).equals(k)) return Optional.of(i); // 1*n
        }
        return Optional.empty(); // 1
    } // T(n) = n+1 + n + 1 = 2n+2, O(n), Omega(1)

    /**
     * Faz pesquisa por um elemento em uma lista, indicando sua
     * posição na lista, se houver o elemento.
     *
     * Se houver mais de um k em elems, o índice do primeiro é
     * retornado, partindo de 0 para elems.size().
     *
     * Limitação: Utilize apenas quando tiver certeza de haver
     * k em elems.
     *
     * @param elems Lista onde k será procurado.
     * @param k Elemento a ser procurado em elems.
     * @return índice de k em elems.
     */
    public static int pesquisaSequencialRapida(List<Integer> elems, Integer k) {
        int i = 0;
        while (true){
            if (elems.get(i).equals(k)) return i;
            i++;
        }
    }

    /**
     * Faz pesquisa por um elemento em uma lista, indicando sua
     * posição na lista, se houver o elemento. Se o elemento não
     * existir, retorna vazio.
     *
     * Se houver mais de um k em elems, o índice do primeiro é
     * retornado, partindo de 0 para elems.size().
     *
     * @param elems Lista onde k será procurado.
     * @param k Elemento a ser procurado em elems.
     * @return índice de k em elems, senão vazio.
     */
    public static Optional<Integer> pesquisaSequencialSentinela(List<Integer> elems, Integer k) {
        final int posFinal = elems.size() - 1; // 1

        // Coloca sentinela no final da lista
        Integer sentinela = elems.get(posFinal); // 1
        elems.set(posFinal, k); // 1

        int i = 0; // 1
        while (elems.get(i).equals(k)) i++; // n

        elems.set(posFinal, sentinela); // 1
        return (i != posFinal || elems.get(posFinal).equals(k)) // 1
                ? Optional.of(i)
                : Optional.empty();
    } // n=elems.size(), T(n) = n + 6, O(n), Omega(1)

    public static void testarPesquisas() {
        List<Integer> vals = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        Optional<Integer> idx = pesquisaSequencial(vals, 0);
        System.out.println(idx.isPresent()
                ? idx.get()
                : "Não houve ocorrência do valor buscado.");
    }
}
