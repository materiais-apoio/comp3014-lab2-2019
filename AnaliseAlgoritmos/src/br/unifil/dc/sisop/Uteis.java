package br.unifil.dc.sisop;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Uteis {

    public static <T extends Comparable<T>> boolean isCrescente(List<T> vs) {
        return isClassificado(vs, (l, r) -> l.compareTo(r));
    }

    public static <T> boolean isClassificado(List<T> vs, Comparator<T> cmp) {
        for (int i = 1; i < vs.size(); i++) {
            if (cmp.compare(vs.get(i-1), vs.get(i)) > 0)
                return false;
        }
        return true;
    }

    public static <T> void permutar(List<T> elems, int i, int j) {
        T held = elems.get(i); // held = elems[i];
        elems.set(i, elems.get(j)); // elems[i] = elems[j];
        elems.set(j, held); // elems[j] = held;
    } // T(n) = 3, (1)

    public static List<Integer> enumerar(int s, int f, int step) {
        Comparator<Integer> ord = s < f
                ? Comparator.naturalOrder()
                : Comparator.reverseOrder();

        List<Integer> vals = new ArrayList<>();
        for (int i = s; ord.compare(i, f) < 0; i+=step)
            vals.add(i);
        return vals;
    }

}
