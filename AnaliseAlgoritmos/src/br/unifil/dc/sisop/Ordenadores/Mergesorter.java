package br.unifil.dc.sisop.Ordenadores;

import br.unifil.dc.sisop.Uteis;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Mergesorter {

    public static void testarMergesort() {
        System.out.println("Mergesort imutável:");
        List<Integer> vals = new ArrayList<>(
                List.of(7,5,10,12,14,0,-5));
        vals = Mergesorter.mergesortComCopia(vals);
        System.out.println(vals.toString());
    }

    /**
     * Retorna uma lista com os valores intercalados de l e r,
     * em ordem classificada, de acordo com cmp.
     *
     * PRECOND: l e r estão pré-ordenadas de acordo com cmp.
     *
     * @param l Um lista classificada.
     * @param r Outra lista classificada.
     * @param cmp Função de classificação.
     * @return Lista com elementos de l e r intercalados por cmp.
     */
    public static <T> List<T> intercalarComCopia(List<T> l, List<T> r, Comparator<T> cmp) {
        assert Uteis.isClassificado(l, cmp) && Uteis.isClassificado(r, cmp) :
                "Listas não estão pré-classificadas para correção do método";

        List<T> intercalado = new ArrayList<>();
        int idxL = 0, idxR = 0;
        while (idxL < l.size() && idxR < r.size()) { // (l + r - 1)
            if (cmp.compare(l.get(idxL), r.get(idxR)) <= 0) {
                intercalado.add(l.get(idxL));
                idxL++;
            } else {
                intercalado.add(r.get(idxR));
                idxR++;
            }
        }

        for (;idxL < l.size(); idxL++) intercalado.add(l.get(idxL)); // (1)
        for (;idxR < r.size(); idxR++) intercalado.add(r.get(idxR)); // (1)

        return intercalado;
    }

    /**
     * Classifica vals por cmp.
     *
     * @param vals Lista a ser classificada, imutável.
     * @return Nova instância de lista com elementos de vals,
     *  classificados por cmp.
     */
    public static <T extends Comparable<T>> List<T> mergesortComCopia(List<T> vals) {
        return mergesortComCopia(vals, (a,b) -> a.compareTo(b)); // (1) *
    }
    public static <T> List<T> mergesortComCopia(List<T> vals, Comparator<T> c) {
        final int valsSize = vals.size();

        // Caso base
        if (valsSize <= 1) return vals;

        // Divisão e conquista
        List<T> left = mergesortComCopia(vals.subList(0, valsSize/2), c);
        List<T> right = mergesortComCopia(vals.subList(valsSize/2, valsSize), c);
        return intercalarComCopia(left, right, c); // Theta(log_2 n) * Theta(n)
    } // Theta(log_2 n) * Theta(n) = Theta(n * log_2 n)



    /**
     * Classifica vals, in-place, de acordo com a ordem natural de T.
     *
     * @param vals Lista a ser classificada.
     */
    public static <T extends Comparable<T>> void mergesort(List<T> vals) {
        mergesort(vals, (a,b) -> a.compareTo(b));
    }

    /**
     * Classifica vals por cmp, in-place.
     *
     * @param vals Lista a ser classificada.
     * @param c Função de classificação.
     */
    public static <T> void mergesort(List<T> vals, Comparator<T> c) {
        mergesort(vals, 0, vals.size(), c);
    }

    private static <T> void mergesort(List<T> vals,
                                      int l, int r, Comparator<T> c) {
        assert r >= l;
        assert r <= vals.size();

        // Caso base
        if (vals.size() <= 1) return;

        // Caso subdivisão
        throw new RuntimeException("O aluno ainda não implementou.");
    }

    /**
     * Intercalador de valores em lista (in-place).
     *
     * @param vals Lista com subdominio a ser intercalado.
     * @param l índice de vals onde inicia o subconjunto da esquerda.
     * @param r índice de vals onde termina o subconjunto da direita.
     */
    private static <T> void intercalar(List<T> vals,
                                       int l, int r, Comparator<T> c) {

        // índice de vals onde termina o subconjunto da esquerda e inicia o da direita.
        final int m = (l+r) / 2;

        throw new RuntimeException("O aluno ainda não implementou.");
    }
}
