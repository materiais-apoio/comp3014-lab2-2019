package br.unifil.dc.sisop.Benchmarking;

import br.unifil.dc.sisop.Ordenadores.Iterativos;
import br.unifil.dc.sisop.Uteis;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Benchmarking {


    /**
     * Implementação totalmente concreta e invariável de medição.
     */
    public static long medirOrdenacao(int qtdeElems) {

        // 0. Inicializar estruturas de dados
        final List<Integer> vals = Uteis.enumerar(0,qtdeElems,1);

        // 1. Marcar tempo do inicio
        final long inicio = System.nanoTime();

        // 2. Executar procedimento em medição
        Iterativos.ordenarSelecao(vals);

        // 3. Diferença entre inicio e fim é o tempo de operação
        return System.nanoTime() - inicio;
    }

    /**
     * Mede o tempo em nano segundos de aplicação de procedimento à
     * estrutura de dados criada por inicializadorDados. A execução de
     * inicializadorDados não é medida.
     *
     * @param inicializadorDados
     * @param procedimento
     * @return tempo em segundos
     */
    public static double medirOrdenacao(
            Supplier<List<Integer>> inicializadorDados,
            Consumer<List<Integer>> procedimento) {

        // 0. Inicializar estruturas de dados
        final List<Integer> vals = inicializadorDados.get();

        // 1. Marcar tempo do inicio
        final double inicio = System.nanoTime();

        // 2. Executar procedimento em medição
        procedimento.accept(vals);

        // 3. Diferença entre inicio e fim é o tempo de operação
        return (System.nanoTime() - inicio) * 1e9;
    }

    public static TabelaTempos tabelarBenchmarkingOrdenacoes(
            int tamInicial, int tamFinal, int passo, int repeticoes) {
            //List<Consumer<List<Integer>>> metodosOrdenacaoListas) {

        TabelaTempos tt = new TabelaTempos();
        tt.setTitulo("Medição de tempos de selectionsort");
        tt.setEtiquetaY("Tempo (ms)");
        tt.setEtiquetaX("Qtde. elementos");
        tt.setLegendas("Selectionsort","Bubblesort");

        for (int i = tamInicial; i < tamFinal; i += passo) {
            final int qtdeElems = i;

            Supplier<List<Integer>> geraOrdenado = () -> Uteis.enumerar(0, qtdeElems,1);
            Supplier<List<Integer>> geraDesordenado = () -> Uteis.enumerar(qtdeElems, 0,-1);

            double tempoTotalSelecao= 0;
            for (int r = 0; r < repeticoes; r++) {
                tempoTotalSelecao += medirOrdenacao(
                        geraDesordenado,
                        Iterativos::ordenarSelecao);
            }

            double tempoTotalBolha= 0;
            for (int r = 0; r < repeticoes; r++) {
                tempoTotalBolha += medirOrdenacao(
                        geraDesordenado,
                        Iterativos::ordenarBolha);
            }

            tt.anotarAmostra(qtdeElems,
                    tempoTotalSelecao,
                    tempoTotalBolha);
        }

        return tt;
    }

    /*public static TabelaTempos tabelarBenchmarkingOrdenacoesFP(
            int tamInicial, int tamFinal, int passo, int repeticoes) {

        TabelaTempos tt = new TabelaTempos();

        IntStream.range(tamInicial/passo, tamFinal/passo)
                .mapToLong((qtdeElems) -> IntStream.range(0,repeticoes)
                    .mapToLong((r) -> medirOrdenacao(
                                    () -> Uteis.enumerar(0, qtdeElems, 1),
                                    Iterativos::ordenarSelecao))
                    .sum())
                .boxed()
                .collect(Collectors.toList());

        return tt;
    }*/
}
