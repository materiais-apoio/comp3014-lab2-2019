package br.unifil.dc.sisop;

import br.unifil.dc.sisop.Benchmarking.Benchmarking;
import br.unifil.dc.sisop.Benchmarking.TabelaTempos;
import br.unifil.dc.sisop.Ordenadores.Iterativos;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;


public class Main {

    // Design pattern Functor ou Visitor.
    public static <T, R> List<R> mapList(
            List<T> xs, Function<T, R> f) {

        List<R> xsCopia = new ArrayList<>();
        for (T x : xs) xsCopia.add(f.apply(x));
        return xsCopia;
    }

    private static Integer soma1(Integer v) { return v+1; }

    public static void main(String[] args) {

        TabelaTempos tt = Benchmarking.tabelarBenchmarkingOrdenacoes(
                500,5000,250,10);
        tt.exibirGraficoXY();
    }

    private static void comoInvocarParamsFuncionais() {
        System.out.println(
            Benchmarking.medirOrdenacao(
                    () -> Uteis.enumerar(0,100,1),
                    Iterativos::ordenarSelecao));
    }

    private static void testarTabelaTempos() {
        TabelaTempos tt = new TabelaTempos();
        tt.setTitulo("Uma tabela teste");
        tt.setEtiquetaX("Qtde. elems");
        tt.setEtiquetaY("Tempo (ms)");
        tt.setLegendas("OrdA", "OrdB");

        // Tabulação
        tt.anotarAmostra(100, 20, 12);
        tt.anotarAmostra(200, 13, 8);
        tt.anotarAmostra(300, 8, 8);
        tt.anotarAmostra(400, 11, 8);
        tt.anotarAmostra(500, 14, 8);
        tt.anotarAmostra(600, 13, 9);
        tt.anotarAmostra(700, 18, 8);
        tt.anotarAmostra(800, 25, 10);
        tt.anotarAmostra(900, 33, 11);

        tt.exibirGraficoXY();
    }

    public static int fib(int n){
        int f0 = 0, f1 = 1; // 1
        while(n-- > 0) { // n + 1
            final int fn = f0 + f1; // 1 * n
            f0 = f1; // 1 * n
            f1 = fn; // 1 * n
        }
        return f0; // 1
    } // n: valor int de n, T(n) = n+1 +n +n+n+1+1 = 4n+3

    public static void helloWorld() {
        System.out.println("Hello World!"); // 1 * T_println("Hello World")
    } // T() = 1
}
